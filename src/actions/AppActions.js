import { SAVE_CURRENT_USER, GET_MOVIE_ITEM, ADD_MORE_MOVIE_ITEM_INFO } from '../constants/AppConstants';

export const saveCurrentUser = ({ username, password, firstname, lastname, token }) => ({
    type: SAVE_CURRENT_USER,
    payload: { username, password, firstname, lastname, token },
})
export const getMovieItem = ({
    name, duration, image, id, date, allShowTime }) => ({
        type: GET_MOVIE_ITEM,
        payload: {
            name, duration, image, id, date, allShowTime
        }
    })
// export const addMoreMovieItemInfo = ({ date, allShowTime })=>({
//     type: ADD_MORE_MOVIE_ITEM_INFO,
//     payload: {date, allShowTime  }
// })

