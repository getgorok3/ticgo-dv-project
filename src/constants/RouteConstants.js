export const ROUTE_TO_LOGIN = '/'
export const ROUTE_TO_REGISTER = '/register'
export const ROUTE_TO_MOVIES = '/Movies'
export const ROUTE_TO_MOVIE_DETAIL = '/MovieDetail'