import { GET_MOVIE_ITEM, ADD_MORE_MOVIE_ITEM_INFO } from '../constants/AppConstants';

const DEFAULT_STATE = {
    name: '',
    duration: '',
    image: '',
    id: '',
    date: '',
    // allShowTime: [{
    //     cinemaName: '',
    //     startThisShowTime: '',
    //     endThisShowTime: '',
    //     soundtrack: '',
    //     subtitle: '',
    //     seats: []
    // }]
    allShowTime: []
}

export default (state = DEFAULT_STATE, { type, payload }) => {
    switch (type) {
        case GET_MOVIE_ITEM:
            return { ...state, ...payload }
            
        case ADD_MORE_MOVIE_ITEM_INFO:
            return { ...state, ...payload }
        default:
            return state
    }
}