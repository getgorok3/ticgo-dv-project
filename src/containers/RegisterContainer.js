import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { ROUTE_TO_LOGIN } from "../constants/RouteConstants";
import RegisterPage from "../pages/Register";

const mapDispatchToProps = dispatch => ({
    backToLogin: () => {
        dispatch(push(ROUTE_TO_LOGIN))
    },
})
export default connect(
    null,
    mapDispatchToProps
)(RegisterPage)