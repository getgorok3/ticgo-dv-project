import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import {ROUTE_TO_REGISTER, ROUTE_TO_MOVIES } from "../constants/RouteConstants";
import LoginPage from "../pages/Login";
import { saveCurrentUser } from "../actions/AppActions";

const mapStateToProps = ({ userAccount }) => ({
    username: userAccount.username,
    password: userAccount.password,
    firstname: userAccount.firstname,
    lastname: userAccount.lastname,
    token: userAccount.token
})

const mapDispatchToProps = dispatch => ({
    onSuccess: user => {
        dispatch(saveCurrentUser(user))
    },
    goToRegister: ()=> {
        dispatch(push(ROUTE_TO_REGISTER))
    },
    goToMovieItems: ()=>{
        dispatch(push(ROUTE_TO_MOVIES))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginPage)