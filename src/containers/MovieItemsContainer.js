import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import MovieItemsPage from '../pages/MovieItems'
import { getMovieItem } from "../actions/AppActions";
import {ROUTE_TO_MOVIE_DETAIL } from "../constants/RouteConstants";
const mapStateToProps = ({ movieItem }) => ({
    name: movieItem.name,
    duration: movieItem.duration,
    image: movieItem.image,
    id: movieItem.id,
    allShowTime: movieItem.allShowTime
   
})

const mapDispatchToProps = dispatch => ({
    onPressMovieItem: movie => {
        dispatch(getMovieItem(movie))
    },
    goToMovieItemDetail: ()=>{
        dispatch(push(ROUTE_TO_MOVIE_DETAIL))
    }
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MovieItemsPage)