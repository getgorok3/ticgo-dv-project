import { connect } from 'react-redux'
import  MovieItemDetailPage  from "../pages/MovieItemDetail";

const mapStateToProps = ({ movieItem }) => ({
    name: movieItem.name,
    duration: movieItem.duration,
    image: movieItem.image,
    id: movieItem.id,
    allShowTime: movieItem.allShowTime

})

export default connect(
    mapStateToProps,
    null
)(MovieItemDetailPage)