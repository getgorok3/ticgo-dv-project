export { default as LoginPage } from './LoginContainer'
export { default as RegisterPage } from './RegisterContainer'
export { default as MovieItemsPage } from './MovieItemsContainer'
export { default as MovieItemDetailPage } from './MovieItemDetailContainer'