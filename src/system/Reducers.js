import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import userInfoReducer from "../reducers/userInfoReducer";
import movieItemReducer from '../reducers/movieItemReducer'

const reducers = history =>
    combineReducers({
        userAccount: userInfoReducer,
        movieItem: movieItemReducer,
        router: connectRouter(history)
    })

export default reducers