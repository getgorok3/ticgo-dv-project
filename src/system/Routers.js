import React, { Component } from 'react'
import { Route, Switch } from 'react-router-native'
import { ROUTE_TO_LOGIN, ROUTE_TO_REGISTER, ROUTE_TO_MOVIES,ROUTE_TO_MOVIE_DETAIL } from "../constants/RouteConstants";
import { LoginPage, RegisterPage, MovieItemsPage, MovieItemDetailPage } from '../containers/index.js'


export default class Router extends Component {
    render() {
        return (
            <Switch>
                <Route exact path={ROUTE_TO_LOGIN} component={LoginPage} />
                <Route exact path={ROUTE_TO_REGISTER} component={RegisterPage} />
                <Route exact path={ROUTE_TO_MOVIES} component={MovieItemsPage} />
                <Route exact path={ROUTE_TO_MOVIE_DETAIL} component={MovieItemDetailPage} />
            </Switch>
        )
    }
}
