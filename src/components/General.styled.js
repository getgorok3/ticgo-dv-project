import styled from 'styled-components'
import { Button, InputItem, Icon } from '@ant-design/react-native';

export const Center = styled.View`
    align-items: center;
    justify-content: center;
`
export const Container = styled.View`
    flex: 1;    
`
export const FooterContainer = styled.View`
    flex-direction: row;
    backgroundColor: #f2f2f2
    padding-top: 5;
    padding-bottom: 5;
    align-items: center;
    height: 50
`
export const HeaderContainer = styled.View`
    backgroundColor: #f2f2f2
    padding-top: 5;
    padding-bottom: 5;
    height: 50
`
export const HeaderText = styled.Text`
    color: white;
    
`
export const FooterIcon = styled.TouchableOpacity`
    flex: 1
    padding-left: 8;
    align-items: center;
    padding-right: 8;
`
export const NewICon = styled(Icon)`
   color: #00cc99;
   
`
export const NewImage = styled.Image`
    width: 100%;
    height: 100%;
`
// About log in
export const LogoFrame = styled.View`
    width: 150;
    height: 150;
    marginTop: 20;
`
export const NewForm = styled.View`
    width: 80%; 
    height: 63%; 
    marginTop: 60;
    borderRadius: 20; 
    flex-direction: column; 
    paddingRight: 15;
`
export const Title = styled.Text`
    color: white; 
    fontSize: 25;
    fontWeight: bold; 
    textAlign: center;
`

export const NewInput = styled(InputItem)`
    width: 100%; 
    color: white; 
`
export const SubmitButton = styled(Button)`
    width: 40%;
`
export const SimpleText = styled.Text`
    color: white; 
    fontSize: 18;
    textAlign: center;
`
export const ItemFrame = styled.View`
    width: 100; 
    height: 150; 
    marginRight: 5;
    marginLeft: 5;
    marginBottom: 5;
    marginTop: 5;
`
export const Picture = styled.Image`
width: 100%; 
height: 100%; 

`
