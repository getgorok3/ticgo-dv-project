import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native';
import {
    ItemFrame, Picture
} from '../components/General.styled'
class ListItem extends Component {
    render() {
        const { image, name, onPress } = this.props
        return (
            <ItemFrame  resizeMode="stretch">
                <TouchableOpacity onPress={onPress}>
                    <Picture source={{ uri: image }}>   </Picture>
                </TouchableOpacity>
              
            </ItemFrame>
        )
    }
}