import React from 'react'
import { connect } from 'react-redux'
import { Icon,  } from '@ant-design/react-native';
import { View } from "react-native";
import {
    FooterContainer,
    FooterIcon, NewICon
} from '../General.styled'

class Footer extends React.Component {
    render() {
        const { goToList, goToUser, goToAddItem } = this.props
        return (
            <FooterContainer >
                <FooterIcon onPress={goToList} >
                     <NewICon name="home" size='md' />
                </FooterIcon>
                <FooterIcon onPress={goToUser} >
                    <NewICon   name="user" size='md'/>
                </FooterIcon>
            </FooterContainer>
        )
    }
}

export default connect(
    null,
    // dispatch => ({
    //     goToList: state => dispatch(push('/products')),
    //     goToUser: state => dispatch(push('/user')),
    //     goToAddItem: state => dispatch(push('/product/add')),
    // })
    null
)(Footer)
