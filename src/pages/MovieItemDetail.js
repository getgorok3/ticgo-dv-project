import React, { Component } from 'react';
import { View, Text, FlatList, ImageBackground, TouchableOpacity } from 'react-native';

class MovieItemDetailPage extends Component {
    state = {
        name: '',
        image: ''
    }
    UNSAFE_componentWillMount = () => {
        this.setState({ name: this.props.name })
        this.setState({ image: this.props.image })

    }
    render() {
        return (
            // <ImageBackground style={{ flex: 1 }}  source={{ uri: this.state.image}} >
            //     <View style={{backgroundColor: 'white', marginTop: 150, flex: 1, 
            //     marginLeft: '5%', marginRight: '5%', marginBottom: '5%'}} > 
            //         <Text>MovieItem: {this.state.name}</Text>
            //     </View>
            // </ImageBackground>
            <ImageBackground style={{ flex: 1 }} source={{ uri: this.state.image }}>
                <View style={{ flex: 1, marginTop: '40%', backgroundColor: 'white' }}>
                <View style={{backgroundColor: 'red', marginTop:10, position: 'absolute',
                width: 100, height: 150}}></View>
                </View >
            </ImageBackground>
        )
    }
}
export default MovieItemDetailPage