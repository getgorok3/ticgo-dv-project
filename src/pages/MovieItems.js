import React, { Component } from 'react';
import { View, Text, FlatList, ImageBackground, TouchableOpacity } from 'react-native';
import axios from 'axios';
import Footer from '../components/general/Footer';
import {
    Container, HeaderContainer,ItemFrame,Picture,Title
} from '../components/General.styled'
import { images } from "../utilities";
const moment = require('moment');

class MovieItemsPage extends Component {
    state = {
        collection: [],
        today: new Date(),
        result:[]
    }
    UNSAFE_componentWillMount = () => {
        this.getAllMovies()
    }


    getAllMovies = () => {
        let count = 0
        axios
            .get('https://zenon.onthewifi.com/ticGo/movies')
            
            .then(res => {
                // handle success
                console.log('All Movies: ', res.data);
                for (each of res.data) {
                   axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${each._id}`,
                        { params: { date: this.state.today.getTime() }})
                        .then(res=>{
                            this.setState({ result: res.data})
                        })
                        
                        .catch(error => {
                            // handle error
                            console.log('error: ', error.response.data.errors);
                        })
                    this.setState({
                        collection: [...this.state.collection, {
                            id: count,
                            movieItem: each,
                        }]
                    })
                    count = count + 1
                }
            })
            .catch(error => {
                // handle error
                console.log('error: ', error.response.data.errors);
            })
        // .finally(() => {
        //     // always executed
        // });
    }
    pressMovieItem = (id) => {
        const data = this.state.collection[id]
        console.log('Yeah: ', this.state.result)
        let movieObject = {
            name: data.movieItem.name,
            duration: data.movieItem.duration,
            image: data.movieItem.image,
            id: data.movieItem._id,
            allShowTime:  this.state.result
        }

        this.props.onPressMovieItem(movieObject)
        this.props.goToMovieItemDetail()
    }


    renderItem = ({ item }) => {
        return (
            <ItemFrame >
                <TouchableOpacity onPress={() => this.pressMovieItem(item.id)}>
                    <Picture resizeMode="stretch" source={{ uri: item.movieItem.image }}
                        style={{ width: '100%', height: '100%' }} />
                </TouchableOpacity>
            </ItemFrame>
        )
    }
   

    render() {

        console.log('finally ', this.state.collection);
        console.log('Redux ', this.props);
        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg3} >
            <Container>
                <HeaderContainer>
                    <Title style={{  color: '#00cc99'}}>Tic Go</Title>
                </HeaderContainer>
                {this.state.collection !== [] ? (
                    <View style={{flex: 1}}>
                    <Container>
                        <FlatList style={{marginLeft: '5%', marginRight: '5%', marginTop: '10%'}}
                            data={this.state.collection}
                            numColumns={3}
                            keyExtractor={(item) => item.id}
                            renderItem={this.renderItem}
                        />
                       
                    </Container>
                    
                     </View>
                  
                ) : (
                        <Text>Not come</Text>
                    )}
                     <Footer ></Footer>
            </Container>
              </ImageBackground>
        )
    }
}
export default MovieItemsPage