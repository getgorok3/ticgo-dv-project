import React, { Component } from 'react';
import { images } from "../utilities";
import { WhiteSpace, } from '@ant-design/react-native';
import {
    Container, LogoFrame, NewImage, NewForm, Title, NewInput,
    SubmitButton, SimpleText
} from '../components/General.styled'
import { ImageBackground, Alert, TouchableOpacity } from 'react-native';
import axios from 'axios';
import { regular } from "../constants/AppConstants";

class RegisterPage extends Component {
    state = {
        username: '',
        password: '',
        confirmPass: '',
        firstname: '',
        lastname: '',
        iscorrect: false
    }

    onSubmitRegister = async () => {
        console.log(this.state.confirmPass === this.state.password)
        if (this.state.confirmPass === this.state.password && regular.test(this.state.username)) {
            try {
                const response = await axios.post('https://zenon.onthewifi.com/ticGo/users/register', {
                    email: this.state.username,
                    password: this.state.password,
                    firstName: this.state.firstname,
                    lastName: this.state.lastname
                })
                console.log('success: ', response);
                this.setState({ iscorrect: true })

            } catch (error) {
                console.log('error: ', error.response.data.errors);
            }
        }
        if (this.state.iscorrect) {
            Alert.alert('You have successfully registered ')
            this.back()
        } else {
            Alert.alert('Can not registered-in, please try again')
        }
        console.log('after Register : ', this.state)
    }


    back = () => {
        this.props.backToLogin()
    }

    changeValue = (state, value) => this.setState({ [state]: value })

    render() {
        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg} >

                <Container style={{ alignItems: 'center' }}>
                    {/* <LogoFrame >
                        <NewImage style={{ borderRadius: 80 }} source={images.user} />
                    </LogoFrame> */}

                    <NewForm >
                        <Title>Register to Tic GO</Title>
                        <WhiteSpace />
                        <NewInput clear placeholderTextColor='#cccccc'
                            placeholder='Username as email' value={this.state.username}
                            onChangeText={value =>
                                this.changeValue('username', value)
                            }>
                        </NewInput>
                        <WhiteSpace />
                        <NewInput clear placeholderTextColor='#cccccc'
                            type="password" placeholder='Password' value={this.state.password}
                            onChangeText={value =>
                                this.changeValue('password', value)
                            }>
                        </NewInput>
                        <WhiteSpace />
                        <NewInput clear placeholderTextColor='#cccccc'
                            type="password" placeholder='Confirm Password' value={this.state.confirmPass}
                            onChangeText={value =>
                                this.changeValue('confirmPass', value)
                            }>
                        </NewInput>
                        <WhiteSpace />
                        <NewInput clear placeholderTextColor='#cccccc'
                            placeholder='First name' value={this.state.firstname}
                            onChangeText={value =>
                                this.changeValue('firstname', value)
                            }>
                        </NewInput>
                        <WhiteSpace />
                        <NewInput clear placeholderTextColor='#cccccc'
                            placeholder='Last name' value={this.state.lastname}
                            onChangeText={value =>
                                this.changeValue('lastname', value)
                            }>
                        </NewInput>

                        <SubmitButton
                            onPress={this.onSubmitRegister}
                            style={{ marginRight: '30%', marginLeft: '30%', marginTop: 40 }}>
                            <SimpleText style={{ color: '#38b6ff', fontWeight: 'bold' }}>Register</SimpleText>
                        </SubmitButton>
                        <WhiteSpace />
                        <TouchableOpacity onPress={this.back}>
                            <SimpleText >Back to login</SimpleText>
                        </TouchableOpacity>
                    </NewForm>
                </Container>
            </ImageBackground>
        )
    }
}
export default RegisterPage