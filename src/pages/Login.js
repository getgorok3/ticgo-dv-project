import React, { Component } from 'react';
import { ImageBackground, TouchableOpacity, Alert } from 'react-native';
import { images } from "../utilities";
import { WhiteSpace, } from '@ant-design/react-native';
import {
    Container, LogoFrame, NewImage, NewForm, Title, NewInput,
    SubmitButton, SimpleText
} from '../components/General.styled'
import axios from 'axios';
import { regular } from "../constants/AppConstants";

class LoginPage extends Component {
    state = {
        username: 'Gg@gmail.com',
        password: '1234',
        firstname: '',
        lastname: '',
        token: '',
        iscorrect: false

    }
    onSubmitLogin = async () => {
       
        try {
            const response = await axios.post('https://zenon.onthewifi.com/ticGo/users/login', {
                email: this.state.username,
                password: this.state.password,
            })
            console.log('Login success: ', response);
            this.setState({
                token: response.data.user.token, firstname: response.data.user.firstName,
                lastname: response.data.user.lastName
            })
            this.setState({ iscorrect: true })
          
        } catch (error) {
            console.log('error: ', error.response.data.errors);
           
        }
        if (this.state.iscorrect) {
            console.log('after Login : ', this.state)
            this.props.onSuccess(this.state)
            console.log('On Success : ', this.props)
            Alert.alert('You have successfully logged-in')
            this.props.goToMovieItems()
        }else{
            Alert.alert('Can not logged-in, please try again')
        }

    }
    changeValue = (state, value) => this.setState({ [state]: value })

    goToRegister = () => {
        this.props.goToRegister()
    }
    


    render() {
        return (
            <ImageBackground style={{ flex: 1 }} source={images.bg2} >

                <Container style={{ alignItems: 'center' }}>
                    <LogoFrame >
                        <NewImage style={{ borderRadius: 80 }} source={images.logo2} />
                    </LogoFrame>

                    <NewForm >
                        <Title>Wellcome to Tic GO</Title>
                        <WhiteSpace />
                        <NewInput placeholderTextColor='#cccccc' 
                        clear 
                        // placeholder='Username' 
                        // onChangeText={value =>this.changeValue('username', value)}
                        value={this.state.username}
                        >
                        </NewInput>
                        <WhiteSpace />
                        <NewInput placeholderTextColor='#cccccc' 
                        clear type="password" 
                        // placeholder='Password' onChangeText={value => this.changeValue('password', value) }
                        value={this.state.password}>
                        </NewInput>

                        <SubmitButton style={{
                            marginRight: '30%', marginLeft: '30%', marginTop: 40,
                        }} onPress={this.onSubmitLogin}>
                            <SimpleText style={{ color: '#38b6ff', fontWeight: 'bold' }}>LogIn</SimpleText>
                        </SubmitButton>
                        <WhiteSpace />

                        <TouchableOpacity onPress={this.goToRegister}>
                            <SimpleText >Register</SimpleText>
                        </TouchableOpacity>
                        <WhiteSpace />
                        <TouchableOpacity>
                            <SimpleText >Login as Guest</SimpleText>
                        </TouchableOpacity>

                    </NewForm>
                </Container>
            </ImageBackground>
        )
    }
}
export default LoginPage